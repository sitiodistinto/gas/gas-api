class Api::V1::RefuelsController < Api::V1::ApiController
  before_action :set_refuel, only: %i[ show update destroy ]

  # GET /refuels
  def index
    @refuels = Refuel.all

    render json: @refuels
  end

  # GET /refuels/1
  def show
    render json: @refuel
  end

  # POST /refuels
  def create
    @refuel = Refuel.new(refuel_params)

    if @refuel.save
      render json: @refuel, status: :created, location: @refuel
    else
      render json: @refuel.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /refuels/1
  def update
    if @refuel.update(refuel_params)
      render json: @refuel
    else
      render json: @refuel.errors, status: :unprocessable_entity
    end
  end

  # DELETE /refuels/1
  def destroy
    @refuel.destroy!
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_refuel
      @refuel = Refuel.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def refuel_params
      params.require(:refuel).permit(:date, :odometer)
    end
end
