class Refuel < ApplicationRecord
  validates :date, :odometer, :volume, presence: true
  validates :odometer, :volume, numericality: { greater_than: 0.0 }

  validate :price_and_cost, if: :volume
  validate :odometer_and_date

  private

  # price or cost calculation
  def price_and_cost
    if price
      self.cost = price * volume
    elsif cost
      self.price = cost / volume
    end
  end

  # Check odometer and date consistency
  def odometer_and_date
    previous = previous_refuel
    errors.add :base, 'Check the date' if previous && previous.date > date
    return errors.empty?
  end

  # Return previous refuel
  def previous_refuel
    Refuel.where("odometer < ?", self.odometer).order(:odometer).last
  end
  # Return previous full refuel
  def previous_full_refuel
    Refuel.where(full: true).where("odometer < ?", self.odometer).order(:odometer).last
  end
end
