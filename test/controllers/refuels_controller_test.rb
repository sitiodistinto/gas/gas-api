require "test_helper"

class RefuelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @refuel = refuels(:one)
  end

  test "should get index" do
    get refuels_url, as: :json
    assert_response :success
  end

  test "should create refuel" do
    assert_difference("Refuel.count") do
      post refuels_url, params: { refuel: { date: @refuel.date, odometer: @refuel.odometer } }, as: :json
    end

    assert_response :created
  end

  test "should show refuel" do
    get refuel_url(@refuel), as: :json
    assert_response :success
  end

  test "should update refuel" do
    patch refuel_url(@refuel), params: { refuel: { date: @refuel.date, odometer: @refuel.odometer } }, as: :json
    assert_response :success
  end

  test "should destroy refuel" do
    assert_difference("Refuel.count", -1) do
      delete refuel_url(@refuel), as: :json
    end

    assert_response :no_content
  end
end
